$(function(){
    $('.copyProfileLink').click(function(){
        const elem = $(this).find('input[type="text"]').get(0);
        const origSelectionStart = elem.selectionStart;
        const origSelectionEnd = elem.selectionEnd;
        elem.focus();
        elem.setSelectionRange(0, elem.value.length);

        document.execCommand("copy");

        const defaultText = $(this).find('span').text();
        $(this).find('span').text('Copied');
        setTimeout(() => {
            $(this).find('span').text(defaultText);
        },1000);
        elem.setSelectionRange(origSelectionStart, origSelectionEnd);
    });


    $(document).on('click', '.outfitShareBtn, .shareBlockTargets', function(){
        $('.copyBlockTargets').removeClass('active').find('i:first-of-type').removeClass('hide').end().find('i:last-of-type').addClass('hide');
        $('.copyTargetsContainer').addClass('hide');
        if($(this).hasClass('shareBlockTargets')){
            $(this).toggleClass('active').find('i').toggleClass('hide');
        }

        if($(this).parents('.outfitBlock').find('.socialMediaSharingButtons').hasClass('socialButtonsShow')){
            $(this).parents('.outfitBlock').find('.socialMediaSharingButtons').removeClass('socialButtonsShow').find('a').removeClass('socialButtonsShow');
            return;
        }
        $('.outfitBlock').find('.socialMediaSharingButtons').removeClass('socialButtonsShow').find('a').removeClass('socialButtonsShow');
        $(this).parents('.outfitBlock').find('.socialMediaSharingButtons').addClass('socialButtonsShow').find('a').addClass('socialButtonsShow');
    });

    $(document).on('click', '.copyBlockTargets', function(){
        $('.socialMediaSharingButtons').removeClass('socialButtonsShow');
        $('.shareBlockTargets').removeClass('active').find('i:first-of-type').removeClass('hide').end().find('i:last-of-type').addClass('hide');


        $(this).toggleClass('active').find('i').toggleClass('hide');
        $(this).parents('.outfitBlock').find('.copyTargetsContainer').toggleClass('hide');
    });

    $('.profiltImgIcon').click(function (e){
        e.stopPropagation();
       $('label[for="mainImgInput"]').click()
    });

    $('.editBgImage').click(function (){
        $('label[for="coverImgInput"]').click()
    });




    const path = document.location.pathname;


    $('.profileNavItem').click(function(){
        $('.profileNavItem').removeClass('activeItem');
        $('.profileMainTabItem').addClass('hide');
        $(this).addClass('activeItem');
        const target = $(this).data('target');
        $(`.profileMainTabItem[data-tab="${target}"]`).removeClass('hide');
        history.pushState({urlPath : path}, '', `${path}?tab=${target}`);

        if(window.outerWidth < 993){
            const selectedTab = $(`.profileNavItem[data-target="${target}"]`);
            const offset = selectedTab.offset().left - selectedTab.prev().outerWidth() / 2;
            $('.profileMainNav').scrollLeft(offset);
        }

    });



    const sanitizeTabName = (tabName) => {
        return $(`.profileNavItem[data-target="${tabName}"]`).length ? tabName : 'created-outfits';
    };

    const getTab = () => {
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        const tabName = String(urlParams.has('tab') && urlParams.get('tab')).toLowerCase();
        return sanitizeTabName(tabName);
    };


    const switchingTab = () => {
        const tabName = getTab();
        const $editTab = $('.profileEditTab');
        const $mainTab = $('.profileMainTabContainer');

        if($editTab.find(`.profileNavItem[data-target="${tabName}"]`).length){
            $editTab.removeClass('hide');
            $mainTab.addClass('hide');
        }else{
            $editTab.addClass('hide');
            $mainTab.removeClass('hide');
        }

        $(`.profileNavItem[data-target="${tabName}"]`).click().parents();
    };

    switchingTab();


    $('.editProfile, .profileName').click(function(){
        $('.profileMainTabContainer').addClass('hide');
        $('.profileEditTab').removeClass('hide');
        history.pushState({urlPath : path}, '', `${path}?tab=edit`);


        switchingTab()
    });

    $('.profileImage').click(function(){
        history.pushState({urlPath : path}, '', `${path}?tab=created-outfits`);
        switchingTab()
    });

    $(document).on('click', '.unfollowBtn', function(){
       $(this).parents('.userBlock').remove();
    });

    $(document).on('click', '.delUploadedFile', function(){
       $(this).parents('.uploadedItem').remove();
    });
});
