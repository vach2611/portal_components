$(function(){

    const _window = $(window);
    const $body = $('body');
    const $filterSpiner = '<div class="filterItemsSpiner"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>';

    const reselAllFiltersButtonHandler = () => {
        $('.selectedFiltersParent').children().length && $('.resetAllFilters').removeClass('hide');
    };

    const closeFiltersDropMenu = () => {
        $('.filterBlock').find('.filterTitle').removeClass('up').addClass('down').end().find('.filterContentContainer').removeClass('active');
    };

    $body.on('click', '.filterTitle', function (event) {
        const $this = $(this);
        const $thisBody = $this.parents('.filterBlock').find('.filterContentContainer');

        if ($thisBody.hasClass('active')) {
            $thisBody.removeClass('active');
            $this.removeClass('up').addClass('down');
            return;
        }

        closeFiltersDropMenu();

        $thisBody.addClass('active');
        $this.removeClass('down').addClass('up');

    });


    const filterDropDownsHandler = e => {
        if (!$('.filterBlock').find(e.target).length) {
            closeFiltersDropMenu();
        }
    };






    $body.on('click' , '.SELECT_ITEM' , function(){
        const $element = $(this);
        const $parent = $element.parents('.filterContentContainer');
        const $applyButton = $parent.find('.filterApply');


        //TODO black checkmark on white color block


        $parent.hasClass('.SINGLE_VALUE') && $element.find('*').removeClass('active');

        $element.find('*').toggleClass('active');

        $parent.find('.active').length
            ? $applyButton.addClass('enabled')
            : $applyButton.removeClass('enabled');
    });



    $body.click(function(e){
       filterDropDownsHandler(e);
    });

    // TODO apped selected filter to .selectedFiltersParent
    // <div class="selectedFilter" data-priceval="2">
    //         <span>20-40 €</span>
    //     <div class="removeFilter">
    //         <i class="fa fa-times" ></i>
    //     </div>
    // </div>



    // TODO .removeFilter functionality


    // TODO .resetAllFilters functionality


    // $(".filterTab").click(function(){
    //     if($(this).hasClass('activeFilterTab')) return;
    //     howToWear = $(this).data('idx');
    //     reloadPage(filter);
    // });




    function filterSearchHandler(){
        const $input = $(this);
        const searchVal = $input.val().toLowerCase();
        const $parent = $input.parents('.filterContentContainer');
        !searchVal
            ? $parent.find('.SELECT_ITEM').show()
            : $parent.find('.SELECT_ITEM').not(`[data-value*="${searchVal}"]`).hide(); $parent.find(`.SELECT_ITEM[data-value*="${searchVal}"]`).show();
    }

    $('.filterSearchInput').on('keyup', filterSearchHandler);





    $('.mobileFiltersButton').on('click', function () {
        $('.filtersBlockParent')
            .addClass('showFiltersModal')
            .prepend(`
                <div class="filtersModalHeader">
                    <button class="closeFiltersModal"><i class="fa fa-times" aria-hidden="true"></i></button>
                    <p class="text-mobile-filter">Filters</p>
                </div>`);
        $body.addClass('preventScrolling');
    });

    $body.on('click', '.closeFiltersModal', function () {
        $('.filtersBlockParent').removeClass('showFiltersModal').find('.filtersModalHeader').remove();
        $body.removeClass('preventScrolling');
    });

    reselAllFiltersButtonHandler();








    if (_window.width() < 993) {

        /////////// ProductListing Page
        const $prodCategoriesContainer = $(".productCategoriesConteiner");
        $prodCategoriesContainer.detach().prependTo(".filtersBlockParent");
        $prodCategoriesContainer.css({'display': 'block'});

        const $productCategoriesList = $('.productCategoriesList');
        if($productCategoriesList.length){
            $productCategoriesList.slick({
                nextArrow: false,
                prevArrow: false,
                dots: false,
                variableWidth: true,
                infinite: false,
                mobileFirst:false,
                slidesToShow: 5,
                slidesToScroll: 1,
                responsive: [
                    {
                        breakpoint: 450,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 2
                        }
                    }
                ],
            });
        }



        /////////// OutfitListing Page
        const $outfitCategoriesContainer = $(".outfitCategoriesConteiner");
        $outfitCategoriesContainer.detach().prependTo(".filtersBlockParent");
        $outfitCategoriesContainer.css({'display': 'block'});

        const $outfitCategoriesList = $('.outfitCategoriesList');
        if($outfitCategoriesList.length){
            $outfitCategoriesList.slick({
                nextArrow: false,
                prevArrow: false,
                dots: false,
                variableWidth: true,
                infinite: false,
                mobileFirst:false,
                slidesToShow: 5,
                slidesToScroll: 1,
                responsive: [
                    {
                        breakpoint: 450,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 2
                        }
                    }
                ],
            });
        }






        ///////Outfit Page
        $('.commentContainer').appendTo('.productsOfCurentOutfitContainer');
        $('.emailSubscribingContainer').appendTo('.productsOfCurentOutfitContainer');




    }




    ///////Outfit Page
    if(_window.width() <= 992 && $('.outfitBlock').length > 4) {
        $('.outfitsContainer').owlCarousel({
            autoWidth: true,
            items: 2,
            margin: 10,
            dots: false,
            loop: false,
            rewind: true,
            autoplay: true,
            autoplayTimeout: 4000,
            nav: false
        });

    }

    $body.on('click', '.commentSentBtn', function(){
        let text = $('.userCommentTextArea').text();

        //TODO create AJAX request for adding comment
    });



    const loadMoreItems = ($element) => {

        if($element.length){


            const startPosition = $element.offset().top + $element.outerHeight() - $(window).height();
            const endPosition = startPosition + 200;
            const scrollPosition = _window.scrollTop();


            if(scrollPosition >= startPosition && scrollPosition < endPosition){
                _window.off("scroll.loadMoreItems", loadMoreItemsHandler);

                //TODO write AJXA request

                // $element.append($html)

                //TODO enable scroll event after AJAX success  (_window.on("scroll.loadMoreItems", loadMoreItemsHandler);)
            }

        }


    };


    //TODO  add another checking into if about items count  (if(_window.width() > 992) && (allItemcCount - existingItemsCount) > 0)
    if(_window.width() > 992){

        _window.on("scroll.loadMoreItems", loadMoreItemsHandler);


        function loadMoreItemsHandler(){
            loadMoreItems($('.outfitsContainer'));
        }



    }








    // FOR "preventDefault inside passive event" ERROR FIX
    const eventListenerOptionsSupported = () => {
        let supported = false;

        try {
            const opts = Object.defineProperty({}, 'passive', {
                get() {
                    supported = true;
                }
            });

            window.addEventListener('test', null, opts);
            window.removeEventListener('test', null, opts);
        } catch (e) {}

        return supported;
    };

    const defaultOptions = {
        passive: false,
        capture: false
    };
    const supportedPassiveTypes = [
        'scroll', 'wheel',
        'touchstart', 'touchmove', 'touchenter', 'touchend', 'touchleave',
        'mouseout', 'mouseleave', 'mouseup', 'mousedown', 'mousemove', 'mouseenter', 'mousewheel', 'mouseover'
    ];
    const getDefaultPassiveOption = (passive, eventName) => {
        if (passive !== undefined) return passive;

        return supportedPassiveTypes.indexOf(eventName) === -1 ? false : defaultOptions.passive;
    };

    const getWritableOptions = (options) => {
        const passiveDescriptor = Object.getOwnPropertyDescriptor(options, 'passive');

        return passiveDescriptor && passiveDescriptor.writable !== true && passiveDescriptor.set === undefined
            ? Object.assign({}, options)
            : options;
    };

    const overwriteAddEvent = (superMethod) => {
        EventTarget.prototype.addEventListener = function (type, listener, options) {
            const usesListenerOptions = typeof options === 'object' && options !== null;
            const useCapture          = usesListenerOptions ? options.capture : options;

            options         = usesListenerOptions ? getWritableOptions(options) : {};
            options.passive = getDefaultPassiveOption(options.passive, type);
            options.capture = useCapture === undefined ? defaultOptions.capture : useCapture;

            superMethod.call(this, type, listener, options);
        };

        EventTarget.prototype.addEventListener._original = superMethod;
    };

    const supportsPassive = eventListenerOptionsSupported();

    if (supportsPassive) {
        const addEvent = EventTarget.prototype.addEventListener;
        overwriteAddEvent(addEvent);
    }

});
