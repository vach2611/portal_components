$(function(){

    const $brandsLoader = `<div class="brandsListLoadingOverlay"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>`;

    const symbols = [
        {target: 'all', html: 'MARKE'},
        {target: '0-9', html: '0-9'},
        {target: 'rest', html: '&#8943;'},
    ];

    const alphabet = [];
    for (let i = 0; i < 26; i++) {
        const letter = (i+10).toString(36);
        alphabet.push({target: `${letter}`, html: `${letter}`});
    }
    symbols.splice(1, 0, ...alphabet);


    const sanitizeSymbol = (symbol) => {
        let validSymbol = 'all';
        for(const {target} of symbols){
            if(target === symbol){
                validSymbol = target;
                break;
            }
        }
        return validSymbol;
    };

    const getSymbol = () => {
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        const symbol = String(urlParams.has('symbol') && urlParams.get('symbol')).toLowerCase();
        return sanitizeSymbol(symbol);
    };






    const pageSymbol = getSymbol();
    let navSymbols = '';
    for(const symbol of symbols){
        const {target, html} = symbol;
        const activeClass = (pageSymbol === target.toLowerCase()) ? 'active' : '';
        navSymbols += `<p data-target="${target}" class="navSimbolBlock ${activeClass}">${html}</p>`
    }
    $('.navigationSimbolsContainer').append(navSymbols);
    $(".navSimbolBlock").first().addClass('all');



// TODO temporary for structure
let x = `<div class="brandBlock" data-id="ra">
            <div class="brandStartingSymbol">A</div>
            <div class="brandsListContainer">
                <div class="brandsList">
                    <a title="Abercrombie & Fitch" href="#"><strong>Abercrombie & Fitch</strong></a>
                    <a title="Abollria" href="#"><strong>Abollria</strong></a>
                    <a title="About Everyone" href="#"><strong>About Everyone</strong></a>
                    <a title="About You" href="#"><strong>About You</strong></a>
                    <a title="About You Curvy" href="#"><strong>About You Curvy</strong></a>
                    <a title="About You Limited" href="#"><strong>About You Limited</strong></a>
                    <a title="About You X Barbie" href="#"><strong>About You X Barbie</strong></a>
                </div>
                <p class="seeMoreBrands" data-target="a" ><strong>Alle marken mit <span>A</span></strong></p>
            </div>
        </div>`;


    const $brandListing = $('.brandListingResult');


    // Brnads List Initing
    $brandListing.append($brandsLoader);
    new Promise( (resolve, reject) => {
        //TODO AJAX call for first init
        setTimeout( () => {
            $brandListing.append(x);
            $brandListing.find('.brandsListLoadingOverlay').remove();
            resolve()
        }, 3000);

    }).then( () => {
        initScrollSpy();
    });




















    $(document).on('click', '.seeMoreBrands', function(){
        const symbol = sanitizeSymbol( $(this).data('target') );
        const parent = $(`.brandBlock[data-id='${symbol}']`);
        const brandsList = parent.find('.brandsList');
        parent.find('.brandsListContainer').append($brandsLoader).css('background-color', '#fff');

        //TODO AJAX call
        setTimeout(() => {
            brandsList.empty().append(x);
            parent.find('.brandsListLoadingOverlay').remove();
            parent.find('.seeMoreBrands').remove();
        },3000);
    });


    $(document).on('click', '.navSimbolBlock:not(.all)', function(){
        $('.navSimbolBlock').removeClass('active');
        $(this).addClass('active');
        const path = document.location.pathname;


        const symbol = sanitizeSymbol( $(this).data('target') );
        const parent = $(`.brandBlock[data-id='${symbol}']`).removeClass('hide');
        $(`.brandBlock[data-id!='${symbol}']`).addClass('hide');
        const loadMoreBtn = parent.find('.seeMoreBrands');

        if(loadMoreBtn.length){
            const brandsList = parent.find('.brandsList');
            parent.find('.brandsListContainer').append($brandsLoader).css('background-color', '#fff');
            //TODO AJAX call
            setTimeout(() => {
                brandsList.empty().append(y);
                parent.find('.brandsListLoadingOverlay').remove();
                loadMoreBtn.remove();

            },3000);
        }



        if(!parent.length){
            $('.brandListingResult').append($brandsLoader);


            //TODO AJAX call
            setTimeout(() => {
                //TODO call create from scratch fn
            },3000);
        }


        history.pushState({urlPath : path}, '', `${path}?symbol=${symbol}`);

    });



    $(document).on('click', '.navSimbolBlock.all', function(e) {
        $('.navSimbolBlock').removeClass('active');
        $(this).addClass('active');
        const symbol = sanitizeSymbol( $(this).data('target') );
        const path = document.location.pathname;
        const $brandBlock = $(".brandBlock");

        if($brandBlock.length === symbols.length - 1){
            $brandBlock.removeClass('hide')
        }else{
            $brandListing.empty().append($brandsLoader);

            //TODO AJAX call
            setTimeout(() => {
                $brandListing.append(y);
                $brandListing.find('.brandsListLoadingOverlay').remove();
            },3000)
        }
        history.pushState({urlPath : path}, '', `${path}?symbol=${symbol}`);

    });





    function initScrollSpy(){

        const $navigationLinks = $('.navSimbolBlock');
        const $sections = $($(".brandBlock").get().reverse());
        const sectionIdToNavigationLink = {};
        $sections.each(function() {
            const id = $(this).data('id');
            sectionIdToNavigationLink[id] = $(`.navSimbolBlock[data-target="${id}"]`);
        });
        function throttle(fn, interval) {
            let lastCall, timeoutId;
            return function () {
                let now = new Date().getTime();
                if (lastCall && now < (lastCall + interval) ) {
                    clearTimeout(timeoutId);
                    timeoutId = setTimeout(function () {
                        lastCall = now;
                        fn.call();
                    }, interval - (now - lastCall) );
                } else {
                    lastCall = now;
                    fn.call();
                }
            };
        }


        function highlightNavigation() {

            if($sections.hasClass('hide') || $sections.length < symbols.length-1) return;

            const scrollPosition = $(window).scrollTop() + 300;
            $sections.each(function() {
                const currentSection = $(this);
                const sectionTop = currentSection.offset().top;
                if (scrollPosition >= sectionTop) {
                    const id = currentSection.data('id');
                    const $navigationLink = sectionIdToNavigationLink[id];
                    if (!$navigationLink.hasClass('active')) {
                        $(`.brandBlock[data-id!="${id}"]`).find('.brandsListContainer').css('background-color','#e6e6e6');
                        currentSection.find('.brandsListContainer').css('background-color','#fff');
                        $navigationLinks.removeClass('active');
                        $navigationLink.addClass('active');

                        if(window.outerWidth < 769) {
                            $('.navigationSimbolsContainer').animate({
                                scrollTop: ( $navigationLink.prevAll().length * $navigationLink.outerHeight() ) - ( $navigationLink.prev().outerHeight() / 2 )
                            })
                        }
                    }
                    return false;
                }
            });
        }

        if(window.outerWidth < 769){
            $(window).scroll( throttle(highlightNavigation,100) );
        }else{
            $brandListing.scroll( throttle(highlightNavigation,100) );
        }

    }



});
