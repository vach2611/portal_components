$(window).on('beforeunload', function(){
    $('.headerAnimatedBorder').addClass('startAnimation');
});

$(window).on("load", function () {
    $('.headerAnimatedBorder').removeClass('startAnimation');
});

$(function(){
    const $header = $('header');
    $header.next('nav').css('margin-top', $header.outerHeight());
    if(window.outerWidth > 991){
        $header.next('nav').next().length && $header.next('nav').next().css('margin-top', $header.outerHeight())
    }


    const goPreviousScreen = () => {
        $(".mobileLoginActions, .mobileMainMenu").removeClass('hide');
        $('.mobSumMenu').addClass('hide')
    };


    $(".hamburgerMenu").click(function () {
        $(this).toggleClass('opened').attr('aria-expanded', $(this).hasClass('opened'));
        goPreviousScreen();

        $(".mobileNav").fadeToggle(100).css({'display': 'flex', 'top': $header.outerHeight(), 'padding-bottom': $header.outerHeight()});
        $('body').toggleClass('disabledScroll')

    });

    $('.mobMenuItemWithSubmenu').click(function(){
        const $target = $(`.${$(this).data('target')}`);
        $(".mobileLoginActions, .mobileMainMenu").addClass('hide');
        $target.removeClass('hide')
    });

    $('.mobSumMenuTitle').click(function(){
        goPreviousScreen()
    });

    $('.searchBtn').click(function(){
        if(window.outerWidth < 992) {

            $('.signInBlockContainer').addClass('hide');
            $('.mobSearchBlock').toggleClass('hide').css({
                'display': 'flex',
                'top': $header.outerHeight(),
                'padding-bottom': $header.outerHeight()
            })
        }
    });


    $('.loginBlock').on('click mouseover', function(e){
        const $targetBlock = $('.signInBlockContainer');
        if(window.outerWidth < 992){
            if(e.type !== 'click') return;
            $('.mobSearchBlock').addClass('hide');
            $targetBlock.toggleClass('hide').css({
                'display': 'flex',
                'top': $header.outerHeight(),
                'padding-bottom': $header.outerHeight()
            });
        }else{
            if(e.type === "mouseover" && !$targetBlock.hasClass('hide')){
                $targetBlock.removeClass('hide')
            }else{
                $targetBlock.toggleClass('hide')
            }
        }
    });

    let oldVal = null;
    $('.searchInput').on('input focus', function(e){
        const searchValue = $(this).val().trim();
        if(searchValue === '') return;

        const $container =  $(window.outerWidth > 768 ? '.desktopSearchResultContainer' : '.mobileSearchResultContainer').removeClass('hide');

        if(e.type === 'focus' && oldVal === searchValue) return;
        oldVal = searchValue;


        const $spinner = `<p class="text-center" ><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></p>`;
        const $errorMessage = `<p class="text-center" >Result Not Found</p>`;
        $container.empty().append($spinner);

        let content = `
                    <div class="brendContainer">
                        <div class="findedBlockTitle">Marke</div>
                        <div class="findedBlockResults">
                            <a class="findedItem">Nike Swim</a>
                        </div>
                    </div>
                    <div class="categoryContainer">
                        <div class="findedBlockTitle">Kategorien</div>
                        <div class="findedBlockResults">
                            <a class="findedItem">Sneaker</a>
                        </div>
                    </div>
                    <div class="productsContainer">
                        <div class="findedBlockTitle">BEKLEIDUNG</div>
                        <div class="findedBlockResults">
                            <a href="#" class="findedItem">
                                <div class="itemImg">
                                    <img class="img-fluid" src="../GlobalAssets/img/tempImg/4711218.jpeg" alt="">
                                </div>
                                <div class="itemName">Nike Sportswear Broek Zwart</div>
                            </a>
                            <a href="#" class="findedItem">
                                <div class="itemImg">
                                    <img class="img-fluid" src="../GlobalAssets/img/tempImg/4715883.jpeg" alt="">
                                </div>
                                <div class="itemName">Nike Sportswear Muiltjes ''Nike Offcourt'' Wit / Pink</div>
                            </a>
                            <a href="#" class="findedItem">
                                <div class="itemImg">
                                    <img class="img-fluid" src="../GlobalAssets/img/tempImg/4715905.jpeg" alt="">
                                </div>
                                <div class="itemName">NIKE Loopschoen ''AIR ZOOM PEGASUS 36 TRAIL'' Gemengde Kleuren</div>
                            </a>
                            <a href="#" class="findedItem">
                                <div class="itemImg">
                                    <img class="img-fluid" src="../GlobalAssets/img/tempImg/4716222.jpeg" alt="">
                                </div>
                                <div class="itemName">Nike Sportswear Broek Beige / Zwart</div>
                            </a>
                        </div>
                    </div>
                    <div class="outfitsContainer">
                        <div class="findedBlockTitle">OUTFITS</div>
                        <div class="findedBlockResults">
                            <a href="#" class="findedItem">
                                <div class="itemImg">
                                    <img class="img-fluid" src="../GlobalAssets/img/tempImg/1.jpg" alt="">
                                </div>
                                <div class="itemName">
                                    Nike Sportswear Broek Beige / Zwart
                                    <div class="extraItemInfo">outfit-ID: N72H</div>
                                </div>

                            </a>
                            <a href="#" class="findedItem">
                                <div class="itemImg">
                                    <img class="img-fluid" src="../GlobalAssets/img/tempImg/2.jpg" alt="">
                                </div>
                                <div class="itemName">
                                    Nike Sportswear Broek Beige / Zwart
                                    <div class="extraItemInfo">outfit-ID: N72H</div>
                                </div>

                            </a>
                            <a href="#" class="findedItem">
                                <div class="itemImg">
                                    <img class="img-fluid" src="../GlobalAssets/img/tempImg/3.jpg" alt="">
                                </div>
                                <div class="itemName">
                                    Nike Sportswear Broek Beige / Zwart
                                    <div class="extraItemInfo">outfit-ID: N72H</div>
                                </div>

                            </a>
                            <a href="#" class="findedItem">
                                <div class="itemImg">
                                    <img class="img-fluid" src="../GlobalAssets/img/tempImg/4.jpg" alt="">
                                </div>
                                <div class="itemName">
                                    Nike Sportswear Broek Beige / Zwart
                                    <div class="extraItemInfo">outfit-ID: N72H</div>
                                </div>

                            </a>
                        </div>
                    </div>
                    <a href="#" class="toBrendPage">ALLE ERGEBNISSE ANZEIGEN</a>`;

        setTimeout(() => {


                $container.empty().append(content)

        },4000)

    });


    $(".signInBlockHeader > p").click(function(){
        $('.signInBlock').removeClass('hide');
        $('.forgotPasswordBlock').addClass('hide');
        $(".signInBlockHeader > p").removeClass('activeTab');
        $(this).addClass('activeTab');
        const $target = $(this).data('tab');
        $('.signInTab').addClass('hide');
        $(`.${$target}`).removeClass('hide')
    });

    $('.forgotPass').click(function(){
        $('.signInBlock').addClass('hide');
        $('.forgotPasswordBlock').removeClass('hide');
    });


    const $headerNavSlider = $('#headerNavSlider');
    if($headerNavSlider.length){
        $headerNavSlider.slick({
            nextArrow: false,
            prevArrow: false,
            dots: false,
            variableWidth: true,
            infinite: false,
            mobileFirst:false,
            slidesToShow: 5,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 5,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                }
            ],
        });
    }







    $(document).click(function(e){
        const $eTarget = $(e.target);
        const $openedBlock = $('.desktopSearchResultContainer');
        const $loginBlock = $('.signInBlockContainer');

        if(!$openedBlock.hasClass('hide') && !$eTarget.parents().is($openedBlock) && !$eTarget.is($openedBlock) && !$eTarget.is($('.searchInput'))){
            $openedBlock.addClass('hide');
        }
        if(!$loginBlock.hasClass('hide') && !$eTarget.parents().is($loginBlock) && !$eTarget.is($loginBlock) && !$eTarget.parents('.loginBlock').length){
            $loginBlock.addClass('hide');
        }

    });
});
