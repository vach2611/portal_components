const $_CONTAINER = $('#pagination');
const TOTAL_ITEMS_COUNT = +$_CONTAINER.data('total-items-count');
const CURRENT_PAGE = $_CONTAINER.data('current-page') ? +$_CONTAINER.data('current-page') : 1;

const isNumeric = (value) => /^[1-9][0-9]*$/.test(value);

const paginate = (totalItems, currentPage) => {

    const SHOW_ITEMS = 10;
    const MAX_PAGES = (window.outerWidth >= 576) ? 5 : 3;

    let previousPage = null;
    let nextPage = null;
    let totalPages = Math.ceil(totalItems / SHOW_ITEMS);

    // ensure current page isn't out of range
    if (currentPage < 1) {
        currentPage = 1;
    } else if (currentPage > totalPages) {
        currentPage = totalPages;
    }

    let startPage, endPage;
    if (totalPages <= MAX_PAGES) {
        // total pages less than max so show all pages
        startPage = 1;
        endPage = totalPages;
    } else {
        // total pages more than max so calculate start and end pages
        let maxPagesBeforeCurrentPage = Math.floor(MAX_PAGES / 2);
        let maxPagesAfterCurrentPage = Math.ceil(MAX_PAGES / 2) - 1;
        if (currentPage <= maxPagesBeforeCurrentPage) {
            // current page near the start
            startPage = 1;
            endPage = MAX_PAGES;
        } else if (currentPage + maxPagesAfterCurrentPage >= totalPages) {
            // current page near the end
            startPage = totalPages - MAX_PAGES + 1;
            endPage = totalPages;
        } else {
            // current page somewhere in the middle
            startPage = currentPage - maxPagesBeforeCurrentPage;
            endPage = currentPage + maxPagesAfterCurrentPage;
        }
    }

    // array from pages
    let pages = Array.from(Array((endPage + 1) - startPage).keys()).map(i => startPage + i);

    if (currentPage - 1 > 0) {
        previousPage = currentPage - 1;
    }
    if (currentPage + 1 <= totalPages) {
        nextPage = currentPage + 1;
    }

    return {
        currentPage,
        totalPages,
        startPage,
        endPage,
        pages,
        previousPage,
        nextPage
    };
};

const drawPagination = (totalItemsCount, currentPageNumber) => {
    if(!$_CONTAINER.length){
        console.warn("Element with ID - '#pagination' doesn't exists  !!!");
        return;
    }

    if(!isNumeric(totalItemsCount) || !isNumeric(currentPageNumber)){
        console.warn("Non-Numeric values is passed for pagination generator !!!");
        return;
    }


    const {currentPage, startPage, endPage, previousPage, nextPage, pages, totalPages} = paginate(totalItemsCount, currentPageNumber);


    if(totalPages <= 1) return;

    const paginationLeftArrow = !previousPage ? 'disabled' : '';
    const paginationRightArrow = !nextPage ? 'disabled' : '';

    let $paginationBlock = `<div class="paginationBlock">`;

        $paginationBlock += `<div data-page="${previousPage}" class="paginationItem ${paginationLeftArrow}"><i class="fa fa-angle-left"></i></div>`;


        if(startPage > 1){
            $paginationBlock += '<div data-page="1" class="paginationItem">1</div>';

            $paginationBlock += (startPage === 3)
                ? '<div data-page="2" class="paginationItem">2</div>'
                : '<div class="paginationItem disabled">...</div>';
        }

        for(const page of pages){
            const isActive = (page === currentPage) ? 'active' : '';
            $paginationBlock += `<div data-page="${page}" class="paginationItem ${isActive}">${page}</div>`;
        }

        if(totalPages > endPage){

            if(totalPages > (endPage + 1)){
                $paginationBlock += (totalPages > (endPage + 2))
                    ? '<div class="paginationItem disabled">...</div>'
                    : `<div data-page="${totalPages - 1}" class="paginationItem">${totalPages - 1}</div>`;
            }

            $paginationBlock += `<div data-page="${totalPages}" class="paginationItem">${totalPages}</div>`;
        }

        $paginationBlock += `<div data-page="${nextPage}" class="paginationItem ${paginationRightArrow}"><i class="fa fa-angle-right"></i></div></div>`;


    $_CONTAINER.empty().append($paginationBlock);

};

// Init Pagination
drawPagination(TOTAL_ITEMS_COUNT, CURRENT_PAGE);




$(document).on('click', '.paginationItem', function(){

    const $_this = $(this);

    if($_this.hasClass('active') || $_this.hasClass('disabled')) return;

    const page = +$_this.data('page');
    const totalItems = +$('#pagination').data('total-items-count');

    drawPagination(totalItems, page);


});

