$(function(){

    $('.MainSlider').slick({
        dots: true,
        infinite: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 4000,
        speed: 2000,
        responsive: [
              {
                  breakpoint: 577,
                  settings: {
                      dots: false,
                      autoplay: false,
                      prevArrow: false,
                      nextArrow: false
                  }
              }
        ]
    });

});
